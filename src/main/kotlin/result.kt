import kotlin.Result.Companion.failure
import kotlin.Result.Companion.success

fun <T> Result<T>.mapErr(block: (Throwable) -> Throwable): Result<T> =
    exceptionOrNull()?.let(block).let { maybeThrowable ->
        if (maybeThrowable == null) {
            this
        } else {
            failure(maybeThrowable)
        }
    }

fun <T> Result<T>.and(other: Result<T>): Result<T> =
    if (isSuccess) {
        other
    } else {
        this
    }

fun <T, U> Result<T>.andThen(block: (T) -> Result<U>): Result<U> =
    try {
        block(getOrThrow())
    } catch (e: Throwable) {
        failure(e)
    }

fun <T> Result<T>.or(other: Result<T>): Result<T> =
    if (isSuccess) {
        this
    } else {
        other
    }

fun <T> Result<T>.orElse(block: (Throwable) -> Result<T>): Result<T> =
    exceptionOrNull().let {
        if (it == null) {
            this
        } else {
            block(it)
        }
    }

fun <T> Result<T?>.transpose(): Result<T>? =
        if (isSuccess) {
            getOrNull()?.let { success(it) }
        } else {
            failure(exceptionOrNull()!!)
        }
