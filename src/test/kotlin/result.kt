import org.junit.jupiter.api.Assertions
import kotlin.test.Test

class ResultTest {
    @Test
    fun mapErr() {
        Assertions.assertEquals(
            "B",
            Result.failure<Int>(ExceptionA()).mapErr { ExceptionB() }.exceptionOrNull()?.message
        )
    }

    @Test
    fun and() {
        Assertions.assertEquals(
            2,
            Result.success(1).and(Result.success(2)).getOrNull()
        )

        Assertions.assertEquals(
            "A",
            Result.failure<Int>(ExceptionA()).and(Result.success(2)).exceptionOrNull()?.localizedMessage
        )

        Assertions.assertEquals(
            "A",
            Result.success(2).and(Result.failure(ExceptionA())).exceptionOrNull()?.localizedMessage
        )

        Assertions.assertEquals(
            "A",
            Result.failure<Int>(ExceptionA()).and(Result.failure(ExceptionB())).exceptionOrNull()?.localizedMessage
        )
    }

    @Test
    fun andThen() {
        Assertions.assertEquals(
            "2",
            Result.success(1).andThen { Result.success("2") }.getOrNull()
        )

        Assertions.assertEquals(
            "message",
            Result.success(1).andThen { Result.failure<String>(Exception("message")) }.exceptionOrNull()?.localizedMessage
        )
    }

    @Test
    fun or() {
        Assertions.assertEquals(
            1,
            Result.success(1).or(Result.success(2)).getOrNull()
        )

        Assertions.assertEquals(
            2,
            Result.failure<Int>(ExceptionA()).or(Result.success(2)).getOrNull()
        )

        Assertions.assertEquals(
            2,
            Result.success(2).or(Result.failure(ExceptionA())).getOrNull()
        )

        Assertions.assertEquals(
            "B",
            Result.failure<Int>(ExceptionA()).or(Result.failure(ExceptionB())).exceptionOrNull()?.localizedMessage
        )
    }

    @Test
    fun orElse() {
        Assertions.assertEquals(
            1,
            Result.success(1).orElse { _ -> Result.success(2) }.getOrNull()
        )

        Assertions.assertEquals(
            2,
            Result.failure<Int>(ExceptionA()).orElse { _ -> Result.success(2) }.getOrNull()
        )

        Assertions.assertEquals(
            2,
            Result.success(2).orElse { _ -> Result.failure(ExceptionA()) }.getOrNull()
        )

        Assertions.assertEquals(
            "B",
            Result.failure<Int>(ExceptionA()).orElse { _ -> Result.failure(ExceptionB()) }.exceptionOrNull()?.localizedMessage
        )
    }

    @Test
    fun transpose() {
        val x: Int? = 2

        Assertions.assertNotNull(Result.success(x).transpose())
        Result.success(x).transpose().let {
            if (it != null) {
                when {
                    it.isSuccess -> Assertions.assertEquals(2, it.getOrThrow())
                    it.isFailure -> Assertions.fail()
                }
            } else {
                Assertions.fail()
            }
        }
    }
}

class ExceptionA : Throwable("A")
class ExceptionB : Throwable("B")
