import com.jfrog.bintray.gradle.BintrayExtension
import com.jfrog.bintray.gradle.tasks.BintrayUploadTask
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    `maven-publish`
    signing
    kotlin("jvm") version "1.3.40"
    id("com.jfrog.bintray") version "1.8.4"
}

group = "com.gitlab.ragnese"
version = "0.2"
description = "Useful extensions to the stdlib Result monad."

ext {
    set("websiteUrl", "https://gitlab.com/ragnese/kotlin-result-ext")
    set("issueTrackerUrl", "https://gitlab.com/ragnese/kotlin-result-ext/issues")
    set("vcsUrl", "https://gitlab.com/ragnese/kotlin-result-ext.git")
    set("licenseUrl", "https://gitlab.com/ragnese/kotlin-result-ext/blob/master/LICENSE")
    set("license", "LGPL-3.0")
}

repositories {
    mavenCentral()
    jcenter()
}

buildscript {
    repositories {
        jcenter()
    }
}

val junitVersion = "5.3.1"

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
    testImplementation(kotlin("test-common"))
    testImplementation(kotlin("test-annotations-common"))
    testImplementation(kotlin("test-junit5"))
    testImplementation(kotlin("test"))
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.freeCompilerArgs = listOf("-Xallow-result-return-type")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

val SourceSet.kotlin: SourceDirectorySet
    get() = withConvention(KotlinSourceSet::class) { kotlin }

val sourcesJar = tasks.register<Jar>("sourcesJar") {
    from(sourceSets["main"].allSource)
    classifier = "sources"
}

val javadocJar = tasks.register<Jar>("javadocJar") {
    dependsOn(JavaPlugin.JAVADOC_TASK_NAME)
    classifier = "javadoc"
    from(tasks["javadoc"])
}

fun BintrayExtension.pkg(configure: BintrayExtension.PackageConfig.() -> Unit) {
    pkg(delegateClosureOf(configure))
}

fun BintrayExtension.PackageConfig.version(configure: BintrayExtension.VersionConfig.() -> Unit) {
    version(delegateClosureOf(configure))
}

val bintrayUser: String? by project
val bintrayKey: String? by project

bintray {
    user = bintrayUser
    key = bintrayKey
    setPublications("maven")

    pkg {
        repo = "maven"
        name = project.name
        websiteUrl = project.ext["websiteUrl"] as String
        issueTrackerUrl = project.ext["issueTrackerUrl"] as String
        vcsUrl = project.ext["vcsUrl"] as String
        setLicenses(project.ext["license"] as String)

        version {
            name = project.version as String
        }
    }
}

val bintrayUpload by tasks.existing(BintrayUploadTask::class) {
    dependsOn("build")
    dependsOn("generatePomFileForMavenPublication")
    dependsOn(sourcesJar)
    dependsOn(javadocJar)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
            groupId = project.group as String
            artifactId = project.name
            version = project.version as String
            artifact(sourcesJar.get())
            artifact(javadocJar.get())
            pom {
                name.set(project.name)
                description.set(project.description)
                url.set(project.ext["websiteUrl"] as String)
                licenses {
                    license {
                        name.set(project.ext["license"] as String)
                        url.set(project.ext["licenseUrl"] as String)
                    }
                }
                developers {
                    developer {
                        id.set("ragnese")
                        name.set("Rob Agnese")
                        email.set("ragnese@protonmail.com")
                    }
                }
                scm {
                    connection.set("scm:git:https://gitlab.com/ragnese/kotlin-result-ext.git")
                    developerConnection.set("scm:git:git@gitlab.com/ragnese/kotlin-result-ext.git")
                    url.set(project.ext["vcsUrl"] as String)
                }
            }
        }
    }
}

signing {
    useGpgCmd()
    sign(configurations.archives)
}
